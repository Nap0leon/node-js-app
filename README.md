# Node JS App

Node.JS application coded with The Net Ninja tutorial

## Express

Package that keep track of all dependencies.

## Nodemon

Package for developping

Monitor your application files so when it's running and listening. If a change is made, then it can monitor the change and restart the server when saved.

`npm install -g nodemon` 

Will install globaly the nodemon package. 
- Means that no matter what app we are working with, we can use the package.

### How to use ?

Enter the command `nodemon app.js`

## EJS

Lightweight templating engine. 

Able to output data with embeded `<%= something %>` into the **HTML** file.

In order to render views in node.js we need to use `res.render` and not `res.sendFile`.

### Views

As default, it's possible to use html template for dynamic query/data. At the root of the project, create a `views` directory. Node.JS will look into this folder by default.


### How to include partials

With EJS and Node.js it's possible to separe the web app into small parts and include each of them into multiple view. This methods makes it possible to code a navigation and include it in many views. 
- It's then possible to modify one file which will be updated on every view

***How ?***
In the `views` folder, create a repository named `partials`. 

Inside the view where you want to include the partial code, implement it using the tag `<% include <path> %>`.

<br>

## Middleware

Essentialy the code that runs between the resquest and the response. `Express` does have a middleware handler.

Used for assets file. In the file `app.js` which is the node.js app, we use the code 
```js
app.use('/assets', express.static('assets'));
```

This line tell node to route the request to the `/assets` path. Therefore, the `styles.css` request will stop returning an error as the file is in the directory `/assets` !!