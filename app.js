/**
 * Express App developped with The Net Ninja tutorial
 */
var express = require('express');
// Get request = app.get('route',fn)
// POST request = app.post('route',fn)

var app = express();

// Setup the EJS view engine in Node.
app.set('view engine', 'ejs');

// Setup tu usage of a middleware for a static file
app.use('/assets', express.static('assets'));
// @param next : when middleware is finished, go onto the next one
// app.use('/assets', function(req, res, next){
//     console.log(req.url);
//     // tell to go to the next request
//     next();
// });

// Static request
app.get('/', function(req, res){
    res.render('index');
});
app.get('/contact', function(req, res){
    res.render('contact');
});

//Dynamic request
app.get('/profile/:id', function(req,res){
    //Dummy data
    var data = {age: 29, job: 'Programmer', hobbie: ['Coding', 'Boulding', 'Fitness', 'Eating']};

    // Will by default look into a views folder
    // Object as second param
    res.render('profile', {person: req.params.id, data: data});
});

app.listen(3000);
